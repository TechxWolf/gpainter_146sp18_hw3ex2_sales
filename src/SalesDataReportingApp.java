import java.text.NumberFormat;

// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 3
//step 1 make main class and call it SalesDataReportingApp
public class SalesDataReportingApp {

    public static void main(String[] args) {

        NumberFormat currency = NumberFormat.getCurrencyInstance();

        System.out.println("Welcome to the Sales Data Report");

        // Step 2 HARD-CODE the data for each region in a rectangular 2D array
   
        double[][] sales = {
            {1540.00, 2010.00, 2450.00, 1845.00},
            {1130.00, 1168.00, 1847.00, 1491.00},
            {1580.00, 2305.00, 2710.00, 1284.00},
            {1105.00, 4102.00, 2391.00, 1576.00}
        };

        String message
                = "Sales Report, by region and quarter, with total annual sales for all regions: \n"
                + "----------------------------------------------------------------------------- \n"
                + "Region     Q1                 Q2                 Q3                 Q4";

        System.out.println(message);
        // Part 3, print our Quarterly totals           
        for (int i = 0; i < sales.length; i++) {
            int c = i + 1;
            System.out.print(c);
            for (int j = 0; j < sales.length; j++) {

                System.out.print("          " + currency.format(sales[i][j]));

            }//end internal for

            System.out.println("");
        }// end outer for
        System.out.println("");// line for readability

        // Part 4 regional sales sums
        System.out.println("Sales by region: ");
        double regTotal = 0.0;
        for (int i = 0; i < sales.length; i++) {
            for (int j = 0; j < sales[0].length; j++) {

                regTotal = regTotal + sales[i][j];

            }//end internal for
            int c = i + 1;
            System.out.println("Region " + c + ": " + currency.format(regTotal) + "  ");
            regTotal = 0;
        }// end outer for
        System.out.println("");// line for readability

        // Part 5 regional sales sums
        System.out.println("Sales by quarter: ");
        double quartTotal = 0.0;
        for (int i = 0; i < sales.length; i++) {
            for (int j = 0; j < sales.length; j++) {

                quartTotal = quartTotal + sales[j][i];

            }//end internal for
            int c = i + 1;
            System.out.println("Q" + c + ": " + currency.format(quartTotal) + "  ");
            quartTotal = 0;
        }// end outer for
        System.out.println(""); // line for readability

        // Part 6 combined regiona total
        System.out.println("Sales by quarter: ");
        double finalTotal = 0.0;
        for (double[] regionSales : sales) {
            for (double quarterlySales : regionSales) {
                finalTotal = finalTotal + quarterlySales;
            } //end internal for
        }// end outer for
        System.out.println("Total annual sales, all regions: " + currency.format(finalTotal));
        System.out.println("");

    }//end main


}// end class SDRA